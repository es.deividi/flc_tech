@include('layouts.partials.header')
<div class="container">
    <!-- Outer Row -->
    <div class="row justify-content-center">
        <div class="col-xl-10 col-lg-12 col-md-9">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-2">Esqueceu sua senha?</h1>
                                    <p class="mb-4">Basta digitar seu endereço de e-mail abaixo e nós lhe enviaremos um link para redefinir sua senha!</p>
                                </div>
                                @if(session('message'))
                                    <div class="alert alert-info p-2 mb-3">
                                        <p>{{session('message')}}</p>
                                    </div>
                                @endif                            
                                
                                <form class="user" action="{{ route('emailRecuperarSenha') }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <input type="email" class="form-control form-control-user"
                                            id="exampleInputEmail" aria-describedby="emailHelp"
                                            placeholder="Insira seu email" name="email" value="{{ old('email') }}">
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-user btn-block">Recuperar Senha</button>
                                </form>
                                <hr>
                                <div class="text-center">
                                    <a class="small" href="{{ route('formulario_criar_conta') }}">Criar Conta</a>
                                </div>
                                <div class="text-center">
                                    <a class="small" href="{{ route('login') }}">Já tem conta?</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('layouts.partials.footer')