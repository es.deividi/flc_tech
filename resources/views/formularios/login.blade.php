@include('layouts.partials.header')
<div class="container">
    <!-- Outer Row -->
        <div class="col-xl-12 col-lg-12 col-md-9">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Bem Vindo a FLC Tech</h1>
                                </div>

                                @if ($errors->any())
                                    <div class="alert alert-danger p-2 mb-3">
                                        @foreach ($errors->all() as $mensagem_erro)
                                            <p>{{ $mensagem_erro }}</p>
                                        @endforeach
                    
                                    </div>
                                @endif

                                <form class="user" action="{{ route('auth') }}" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <input type="email" class="form-control form-control-user" id="email" name="email" placeholder="Insira seu email" value="{{ old('email') }}" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control form-control-user" id="password" name="password" placeholder="Senha" required>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-user btn-block">Login</button>
                                </form>
                                <hr>
                                <div class="text-center">
                                    <a class="small" href="{{ route('formulario_recuperar_senha') }}">Esqueceu sua senha?</a>
                                </div>
                                <div class="text-center">
                                    <a class="small" href="{{ route('formulario_criar_conta') }}">Criar conta</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('layouts.partials.footer')