@include('layouts.partials.header')
<div class="container">
    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Criar conta</h1>
                        </div>
                        @if ($errors->any())
                            <div class="alert alert-danger p-2 mb-3">
                                @foreach ($errors->all() as $mensagem_erro)
                                    <p>{{ $mensagem_erro }}</p>
                                @endforeach
            
                            </div>
                        @endif
                        <form class="user" action="{{ route('create.user') }}" method="POST">
                            @csrf
                            <div class="form-group row">
                                <div class="col-sm-12 mb-3 mb-sm-0">
                                    <input type="text" class="form-control form-control-user" id="nome" name="nome" placeholder="Nome" value="{{ old('nome') }}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control form-control-user" id="email" name="email" placeholder="Email" value="{{ old('email') }}" required>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <input type="password" class="form-control form-control-user" id="password" placeholder="Senha" name="password" required>
                                </div>
                                <div class="col-sm-6">
                                    <input type="password" class="form-control form-control-user" id="password2" placeholder="Repita a senha" name="password2" required>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary btn-user btn-block">Criar conta</button>
                        </form>
                        <hr>
                        <div class="text-center">
                            <a class="small" href="{{ route('formulario_recuperar_senha') }}">Esqueceu sua senha?</a>
                        </div>
                        <div class="text-center">
                            <a class="small" href="{{ route('login') }}">Já tem conta?</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('layouts.partials.footer')