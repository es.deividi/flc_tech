@include('layouts.partials.adm.header')

<div id="wrapper">

    @include('admin.partials.menu');

    <!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">
    <!-- Main Content -->
    <div id="content">
        @include('admin.partials.topBar')
        <!-- Begin Page Content -->
        <div class="container-fluid">
            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Tipo de Receita</h1>
            </div>

            <!-- Content Row -->
            <div class="row">
                <!-- Content Column -->
                <div class="col-lg-12 mb-4">
                    <!-- Project Card Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Novo tipo de Receita</h6>
                            <div class="text-right" style="margin-top:-16px">
                                <a href="{{ route('listarTiposReceitas') }}"><i class="fas fa-backward"></i></a>
                            </div>
                        </div>
                        
                        @if ($errors->any())
                            <div class="alert alert-danger p-2 mb-3">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                @foreach ($errors->all() as $mensagem_erro)
                                    <p>{{ $mensagem_erro }}</p>
                                @endforeach
            
                            </div>
                        @endif
                        
                        <div class="card-body">
                            <form action="{{ route('salvarTipoReceita') }}" method="post">
                                @csrf
                                <div class="form-group row">
                                    <div class="col-sm-12 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user" id="nome" name="nome" value="{{ old('nome') }}" placeholder="Nome" required>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-user btn-block">Salvar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- End of Main Content -->
    <!-- Footer -->
    @include('admin.partials.copyright')
    <!-- End of Footer -->
</div>
<!-- End of Content Wrapper -->

</div>


@include('layouts.partials.adm.footer')