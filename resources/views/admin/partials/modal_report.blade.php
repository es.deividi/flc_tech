@if(Session::has('report'))
<div class="modal fade" id="modal-report-download" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{Session::get('report_name')}}</h4>
                <button type="button" class="close close-modal" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body text-center">
                <div class="container">
                    <i class="far fa-check-circle"></i> <strong>Arquivo gerado com sucesso!</strong>
                    <br><br>
                    <a type="button" class="btn btn-secondary" href="{{ Session::get('report_link') }}" target='download'><i class="fas fa-cloud-download-alt"></i> Clique aqui para realizar o download.</a>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-primary float-right close-modal" data-bs-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
@endif