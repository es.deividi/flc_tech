<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('dashboard') }}">
        <div class="sidebar-brand-icon">
            {{-- <i class="fas fa-laugh-wink"></i> --}}
            <i class="fas fa-solid fa-coins"></i>
        </div>
        <div class="sidebar-brand-text mx-3">FLC Tech</div>
    </a>
    <!-- Divider -->
    <hr class="sidebar-divider my-0">
    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="{{ route('dashboard') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Painel de controle</span></a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider">
    <!-- Heading -->
    {{-- <div class="sidebar-heading">
        Interface
    </div> --}}
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#receita"
            aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-dollar-sign"></i>
            <span>Receitas</span>
        </a>
        <div id="receita" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header"></h6>
                <a class="collapse-item" href="{{ route('listarReceitas') }}">Consultar</a>
                <a class="collapse-item" href="{{ route('cadastrarReceita') }}">Cadastrar</a>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#tipoReceita"
            aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-dollar-sign"></i>
            <span>Tipos de Receitas</span>
        </a>
        <div id="tipoReceita" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header"></h6>
                <a class="collapse-item" href="{{ route('listarTiposReceitas') }}">Consultar</a>
                <a class="collapse-item" href="{{ route('cadastrarTipoReceita') }}">Cadastrar</a>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#despesas"
            aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-dollar-sign"></i>
            <span>Despesas</span>
        </a>
        <div id="despesas" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header"></h6>
                <a class="collapse-item" href="{{ route('listarDespesas') }}">Consultar</a>
                <a class="collapse-item" href="{{ route('cadastrarDespesa') }}">Cadastrar</a>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#tipoDespesas"
            aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-dollar-sign"></i>
            <span>Tipos de Despesas</span>
        </a>
        <div id="tipoDespesas" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header"></h6>
                <a class="collapse-item" href="{{ route('listarTiposDespesas') }}">Consultar</a>
                <a class="collapse-item" href="{{ route('cadastrarTipoDespesa') }}">Cadastrar</a>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#lembretes"
            aria-expanded="true" aria-controls="collapseTwo">
            <i class="fa fa-calendar"></i>
            <span>Lembretes</span>
        </a>
        <div id="lembretes" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header"></h6>
                <a class="collapse-item" href="{{ route('listarLembretes') }}">Consultar</a>
                <a class="collapse-item" href="{{ route('cadastrarLembrete') }}">Cadastrar</a>
            </div>
        </div>
    </li>
    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>
<!-- End of Sidebar -->