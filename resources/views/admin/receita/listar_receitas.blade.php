@include('layouts.partials.adm.header')
<div id="wrapper">
    @include('admin.partials.menu');
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
        <!-- Main Content -->
        <div id="content">
            @include('admin.partials.topBar')
            <!-- Begin Page Content -->
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Receitas</h1>
                </div>
                <div class="card shadow mb-4">
                    <div class="card-header py-3 text-right">
                        <a href="{{ route('cadastrarReceita') }}">
                            <i class="fas fa-plus"></i>
                        </a>
                    </div>
                    @if(session('message'))
                        <div class="alert alert-info p-2 mb-3">
                            <p>{{session('message')}}</p>
                        </div>
                    @endif
                    
                    @if(session('erro'))
                        <div class="alert alert-danger p-2 mb-3">
                            <p>{{session('erro')}}</p>
                        </div>
                    @endif
                    
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Nome da receita</th>
                                        <th>Tipo de Receita</th>
                                        <th>Valor</th>
                                        <th>Data Recebimento</th>
                                        <th width="100"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($records as $record)
                                        <tr>
                                            <td>
                                                {{$record->NOME}}
                                            </td>
                                            <td>
                                                {{$record->DS_RECEITA}}
                                            </td>
                                            <td>
                                                @if($record->VALOR)
                                                    {{ $record->VALOR }}
                                                @endif
                                            </td>
                                            <td>
                                                {{$record->DATA_RECEBIMENTO}}
                                            </td>
                                            <td class="text-center">
                                                <a href="{{ route('editarReceita', ['id'=> $record->ID]) }}">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                                <a href="{{ route('deleteReceita', ['id'=> $record->ID]) }}">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- End of Main Content -->
        <!-- Footer -->
        @include('admin.partials.copyright')
        <!-- End of Footer -->
    </div>
<!-- End of Content Wrapper -->
</div>
@include('layouts.partials.adm.footer')