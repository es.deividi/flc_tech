@include('layouts.partials.adm.header')
<div id="wrapper">
    @include('admin.partials.menu');
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
        <!-- Main Content -->
        <div id="content">
            @include('admin.partials.topBar')
            <!-- Begin Page Content -->
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Receita</h1>
                </div>
                <!-- Content Row -->
                <div class="row">
                    <!-- Content Column -->
                    <div class="col-lg-12 mb-4">
                        <!-- Project Card Example -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Editar Receita</h6>
                                <div class="text-right" style="margin-top:-16px">
                                    <a href="{{ route('listarReceitas') }}"><i class="fas fa-backward"></i></a>
                                </div>
                            </div>
                            @if(session('message'))
                                <div class="alert alert-info p-2 mb-3">
                                    <p>{{session('message')}}</p>
                                </div>
                            @endif                            
                            
                            @if(session('erro'))
                            <div class="alert alert-danger p-2 mb-3">
                                <p>{{session('erro')}}</p>
                            </div>
                            @endif
                            <div class="card-body">
                                <form action="{{ route('updateReceita') }}" method="post">
                                    @csrf
                                    <div class="form-group row">
                                        <div class="col-sm-6 mb-3 mb-sm-0">
                                            <label>Nome da receita</label>
                                            <input type="text" class="form-control form-control-user" name="id" value="{{$record[0]->ID }}" hidden>
                                            <input type="text" class="form-control form-control-user" id="nome" name="nome" value="{{ old('nome', $record[0]->NOME) }}" placeholder="Nome" required>
                                        </div>
                                        <div class="col-sm-6 mb-3 mb-sm-0">
                                            <label>Tipo de receita</label>
                                            <select class="form-control form-control-user" name="tipoReceita" aria-label="" placeholder="Tipo de receita">
                                                <option value="" selected disabled hidden>Tipo de receita</option>
                                                @foreach ($tiposReceita as $tipoReceita)
                                                    <option value="{{$tipoReceita->ID}}" 
                                                    @if ($record[0]->TIPO_RECEITA == $tipoReceita->ID)
                                                        selected
                                                    @endif    
                                                    >{{$tipoReceita->NOME}}</option>
                                                @endforeach
                                              </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-6 mb-3 mb-sm-0">
                                            <label>Valor</label>
                                            <input type="text" class="form-control valor" name="valor" value="{{ old('valor', $record[0]->VALOR) }}">
                                        </div>
                                        <div class="col-sm-6 mb-3 mb-sm-0">
                                            <label>Data recebimento</label>
                                            <div class="input-group date" id="datepicker">
                                                <input type="text" class="form-control" name="dataRecebimento" value="{{ old('dataRecebimento', $record[0]->DATA_RECEBIMENTO) }}">
                                                <span class="input-group-append">
                                                    <span class="input-group-text bg-white d-block">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <textarea class="form-control form-control-user" name="observacao" placeholder="Observação">{{ old('observacao', $record[0]->OBSERVACAO) }}</textarea>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-user btn-block">Salvar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- End of Main Content -->
        <!-- Footer -->
        @include('admin.partials.copyright')
        <!-- End of Footer -->
    </div>
    <!-- End of Content Wrapper -->
</div>
@include('layouts.partials.adm.footer')