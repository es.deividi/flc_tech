@include('layouts.partials.adm.header')

<div id="wrapper">

    @include('admin.partials.menu');

    @include('admin.partials.main')

</div>

@include('layouts.partials.adm.footer')