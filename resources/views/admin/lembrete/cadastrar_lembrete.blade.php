@include('layouts.partials.adm.header')
<div id="wrapper">
    @include('admin.partials.menu');
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
        <!-- Main Content -->
        <div id="content">
            @include('admin.partials.topBar')
            <!-- Begin Page Content -->
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Lembrete</h1>
                </div>
                <!-- Content Row -->
                <div class="row">
                    <!-- Content Column -->
                    <div class="col-lg-12 mb-4">
                        <!-- Project Card Example -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Novo Lembrete</h6>
                                <div class="text-right" style="margin-top:-16px">
                                    <a href="{{ route('listarLembretes') }}"><i class="fas fa-backward"></i></a>
                                </div>
                            </div>
                            @if(session('message'))
                                <div class="alert alert-info p-2 mb-3">
                                    <p>{{session('message')}}</p>
                                </div>
                            @endif                            
                            
                            @if(session('erro'))
                            <div class="alert alert-danger p-2 mb-3">
                                <p>{{session('erro')}}</p>
                            </div>
                            @endif
                            <div class="card-body">
                                <form action="{{ route('salvarLembrete') }}" method="post">
                                    @csrf
                                    <div class="form-group row">
                                        <div class="col-sm-6 mb-3 mb-sm-0">
                                            <label>Nome</label>
                                            <input type="text" class="form-control form-control-user" id="nome" name="nome" placeholder="Nome" required>
                                        </div>
                                        <div class="col-sm-6 mb-3 mb-sm-0">
                                            <label>Data</label>
                                            <div class="input-group date" id="datepicker">
                                                <input type="text" class="form-control" name="data" required>
                                                <span class="input-group-append">
                                                    <span class="input-group-text bg-white d-block">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-6 mb-3 mb-sm-0">
                                            <label>Email</label>
                                            <input type="email" class="form-control" name="email" value="" required>
                                        </div>
                                        <div class="col-sm-6 mb-3 mb-sm-0">

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <label>Descricao</label>
                                            <textarea class="form-control form-control-user" name="descricao" placeholder="Descrição"></textarea>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-user btn-block">Criar lembrete</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- End of Main Content -->
        <!-- Footer -->
        @include('admin.partials.copyright')
        <!-- End of Footer -->
    </div>
    <!-- End of Content Wrapper -->
</div>
@include('layouts.partials.adm.footer')