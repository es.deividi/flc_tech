    <!-- Bootstrap core JavaScript-->
    <script src="{{ url('') }}/resources/vendor/jquery/jquery.min.js"></script>
    <script src="{{ url('') }}/resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ url('') }}/resources/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ url('') }}/resources/js/sb-admin-2.min.js"></script>
    <script src="{{ url('') }}/resources/js/datepicker.js"></script>
    <script src="{{ url('') }}/resources/js/chart.js"></script>
    
    
    @if (str_contains(url()->current(), '/dashboard'))
    <script>
        const container = $('.chart-area');
        const ctx = $('#chart');
        const labels = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'];
        const myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: labels,
                datasets: [
                    {
                        label: 'Receita',
                        data: [],
                        fill: false,
                        borderColor: 'green',
                        backgroundColor: 'green',
                        tension: 0.4
                    },                    
                    {
                        label: 'Despesa',
                        data: [],
                        fill: false,
                        borderColor: 'red',
                        backgroundColor: 'red',
                        tension: 0.4
                    },
                ],
            },
            options: {
                plugins: {
                    tooltip: {
                        callbacks: {
                            label: function(context) {
                                let label = context.dataset.label || '';

                                if (label) {
                                    label += ': ';
                                }
                                if (context.parsed.y !== null) {
                                    label += new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(context.parsed.y);
                                }
                                return label;
                            }
                        }
                    }
                }
            }
        });
        container.innerHeight(ctx.innerHeight()*1.1);

        (
            function(){
                $.ajax({
                    'processing': true, 
                    'serverSide': true,
                    type: "GET",
                    url: "{{ route('chartData') }}",
                    datatype: "json",
                    success: function(data) {
                        var receitas = data.data.receitas,
                        despesas = data.data.despesas;
                        $.each(receitas, function(index, value) {
                            myChart.data.datasets[0].data.push(value);//Receitas
                        });
                        
                        $.each(despesas, function(index, value) {
                            myChart.data.datasets[1].data.push(value);//Despesas
                        });
                        
                        myChart.update();
                    }
                })    
            }
        )();
    </script>        
    @endif

    <script>
        $(".alert").fadeTo(2000, 500).slideUp(500, function(){
            $(".alert").slideUp(500);
        });

        $(function() {
            $('#datepicker').datepicker({
                format: 'dd/mm/yyyy'
            });

            $(".valor").on("keyup", function(){
                this.value = formataValor(this.value);
            });

            $( ".valor" ).prop("value", function() {
                this.value = formataValor(this.value);
            });

            function formataValor(value){
                var v = value.replace(/\D/g,'');
                v = (v/100).toFixed(2) + '';
                v = v.replace(".", ",");
                v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
                v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
                return v;
            }

            $('#modal-report-download').modal('show');

            $('.close-modal').click(function(){
                $('#modal-report-download').modal('hide');
            });

            $.ajax({
                'processing': true, 
                'serverSide': true,
                type: "GET",
                url: "{{ route('lembretesAlterta') }}",
                datatype: "json",
                success: function(data) {
                    var lembretes = data.data,
                    container = $('#central-lembretes .list'),
                    badge = $('.badge-counter');                    
                    badge.text(lembretes.length),
                    maxItem = lembretes.length <= 4 ? lembretes.length : 3;

                    for(var c = 0; c < maxItem; c++){
                        var record = lembretes[c],
                        data = record.DATA.split("-").reverse().join("/"),
                        url = "{{URL::to('editarLembrete')}}/"+ record.ID +"",
                        link = $("<a class=\"dropdown-item d-flex align-items-center\" href="+url+"></a>"),
                        icon = $("<div class=\"mr-3\"><div class=\"icon-circle bg-primary\"><i class=\"fa fa-calendar text-white\"></i></div></div>"),
                        info = $("<div><div class=\"small text-gray-500\">"+data+"</div><span class=\"font-weight-bold\">"+record.NOME+"</span></div>");

                        $(link).append(icon);
                        $(link).append(info);
                        $(container).append(link);
                    }
                }
            });
        });
    </script>
</body>
</html>