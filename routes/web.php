<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ReceitaController;
use App\Http\Controllers\DespesaController;
use App\Http\Controllers\TipoReceitaController;
use App\Http\Controllers\TipoDespesaController;
use App\Http\Controllers\LembreteController;

Route::get('/', [UsuarioController::class, 'viewFormulariologin'])->name('login');
Route::get('/recuperar', [UsuarioController::class, 'viewRecuperarSenha'])->name('formulario_recuperar_senha');
Route::get('/criarConta', [UsuarioController::class, 'viewCriarConta'])->name('formulario_criar_conta');
Route::post('/emailRecuperarSenha',[UsuarioController::class, 'emailRecuperarSenha'])->name('emailRecuperarSenha');
Route::get('/recuperarSenha',[UsuarioController::class, 'recuperarSenha'])->name('recuperarSenha');
Route::post('/updateSenha',[UsuarioController::class, 'updateSenha'])->name('updateSenha');

Route::post('/createUser', [UsuarioController::class, 'create'])->name('create.user');

Route::post('/auth', [UsuarioController::class, 'auth'])->name('auth');
Route::get('/logout', [UsuarioController::class, 'logout'])->name('logout');
Route::get('/dashboard', [DashboardController::class, 'viewDashboard'])->name('dashboard');
Route::get('/chartData', [DashboardController::class, 'chartData'])->name('chartData');
Route::get('/dashboard/export/', [DashboardController::class, 'export'])->name('dashboardExport');

// Tipo Receita Início
Route::get('/listarTiposReceitas', [TipoReceitaController::class, 'viewListarTiposReceitas'])->name('listarTiposReceitas');
Route::get('/cadastrarTipoReceita', [TipoReceitaController::class, 'viewCadastrarTipoReceita'])->name('cadastrarTipoReceita');
Route::post('/salvarTipoReceita', [TipoReceitaController::class, 'salvarTipoReceita'])->name('salvarTipoReceita');
Route::post('/updateTipoReceita', [TipoReceitaController::class, 'updateTipoReceita'])->name('updateTipoReceita');
Route::get('/editarTipoReceita/{id}', [TipoReceitaController::class, 'viewEditarTipoReceita'])->name('editarTipoReceita');
Route::get('/deleteTipoReceita/{id}', [TipoReceitaController::class, 'deleteTipoReceita'])->name('deleteTipoReceita');
// Tipo Receita Fim

// Tipo Despesa Início
Route::get('/listarTiposDespesas', [TipoDespesaController::class, 'viewListarTiposDespesas'])->name('listarTiposDespesas');
Route::get('/cadastrarTipoDespesa', [TipoDespesaController::class, 'viewCadastrarTipoDespesa'])->name('cadastrarTipoDespesa');
Route::post('/salvarTipoDespesa', [TipoDespesaController::class, 'salvarTipoDespesa'])->name('salvarTipoDespesa');
Route::post('/updateTipoDespesa', [TipoDespesaController::class, 'updateTipoDespesa'])->name('updateTipoDespesa');
Route::get('/editarTipoDespesa/{id}', [TipoDespesaController::class, 'viewEditarTipoDespesa'])->name('editarTipoDespesa');
Route::get('/deleteTipoDespesa/{id}', [TipoDespesaController::class, 'deleteTipoDespesa'])->name('deleteTipoDespesa');
// Tipo Despesa Fim

// Receita Início
Route::get('/listarReceitas', [ReceitaController::class, 'viewListarReceitas'])->name('listarReceitas');
Route::get('/cadastrarReceita', [ReceitaController::class, 'viewCadastrarReceita'])->name('cadastrarReceita');
Route::post('/salvarReceita', [ReceitaController::class, 'salvarReceita'])->name('salvarReceita');
Route::post('/updateReceita', [ReceitaController::class, 'updateReceita'])->name('updateReceita');
Route::get('/editarReceita/{id}', [ReceitaController::class, 'viewEditarReceita'])->name('editarReceita');
Route::get('/deleteReceita/{id}', [ReceitaController::class, 'deleteReceita'])->name('deleteReceita');
// Receita Fim

// Despesa Início
Route::get('/listarDespesas', [DespesaController::class, 'viewListarDespesas'])->name('listarDespesas');
Route::get('/cadastrarDespesa', [DespesaController::class, 'viewCadastrarDespesa'])->name('cadastrarDespesa');
Route::post('/salvarDespesa', [DespesaController::class, 'salvarDespesa'])->name('salvarDespesa');
Route::post('/updateDespesa', [DespesaController::class, 'updateDespesa'])->name('updateDespesa');
Route::get('/editarDespesa/{id}', [DespesaController::class, 'viewEditarDespesa'])->name('editarDespesa');
Route::get('/deleteDespesa/{id}', [DespesaController::class, 'deleteDespesa'])->name('deleteDespesa');
// Despesa Fim

// Lembrete Início
Route::get('/listarLembretes', [LembreteController::class, 'viewListarLembretes'])->name('listarLembretes');
Route::get('/cadastrarLembrete', [LembreteController::class, 'viewCadastrarLembrete'])->name('cadastrarLembrete');
Route::post('/salvarLembrete', [LembreteController::class, 'salvarLembrete'])->name('salvarLembrete');
Route::post('/updateLembrete', [LembreteController::class, 'updateLembrete'])->name('updateLembrete');
Route::get('/editarLembrete/{id}', [LembreteController::class, 'vieweditarLembrete'])->name('editarLembrete');
Route::get('/deleteLembrete/{id}', [LembreteController::class, 'deleteLembrete'])->name('deleteLembrete');
Route::get('/lembretesAlterta', [LembreteController::class, 'lembretesAlterta'])->name('lembretesAlterta');

// Lembrete Fim