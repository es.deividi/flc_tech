<?php
namespace  App\Reports;

use App\Models\ModelUtil;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use function Psy\debug;

class Reports{

    public function reportDashboard($data){
        $util = new ModelUtil();
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Quadro financeiro');
        $sheet->getStyle('A2:M2')->getBorders()->getOutline()->setBorderStyle(true);
        
        $sheet->setCellValue('A1', 'Nome');
        $sheet->setCellValue('B1', session()->get('Auth')['nome']);
        $sheet->setCellValue('A2', 'JAN');
        $sheet->setCellValue('B2', 'FEV');
        $sheet->setCellValue('C2', 'MAR');
        $sheet->setCellValue('D2', 'ABR');
        $sheet->setCellValue('E2', 'MAI');
        $sheet->setCellValue('F2', 'JUN');
        $sheet->setCellValue('G2', 'JUL');
        $sheet->setCellValue('H2', 'AGO');
        $sheet->setCellValue('I2', 'SET');
        $sheet->setCellValue('J2', 'OUT');
        $sheet->setCellValue('K2', 'NOV');
        $sheet->setCellValue('L2', 'DEZ');
        $sheet->setCellValue('M2', 'ANO');
        $linha = 3;

        foreach($data as $items){
            $letra = 'A';
            foreach($items as $item){
                $sheet->setCellValue($letra.$linha, $util->formatarValor($item));
                $letra++;
            }
            $linha++;
        }
        
        $sheet->getStyle('A2:M'.$linha)->getAlignment()->setHorizontal('center');
        
        $writter = new Xlsx($spreadsheet);
        $filename = "report-".time().".xlsx";
        $writter->save(storage_path('app/public/reports/'.$filename));
        $retorno = [
            'report' => true,
            'report_link' => asset('public/storage/reports/'.$filename),
            'report_name' => 'Quadro financeiro',
            'fileName' => $filename
        ];
        return $retorno;
    }

}