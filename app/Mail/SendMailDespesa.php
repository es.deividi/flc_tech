<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMailDespesa extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->info = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $info = $this->info;
        $data = [
            'titulo' => 'Lembrete de vencimento',
            'texto1' => 'Sua despesa('.$info->NOME.') vence amanhã.',
            'texto2' => 'Para consultar clique no botão abaixo!',
            'rota' => route('editarDespesa',['id'=> $info->ID]),
        ];
        return $this->from('to@email.com')->subject('Lembrete de vencimento')->view('emails.lembrete', $data );
    }
}
