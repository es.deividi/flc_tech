<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMailLembrete extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->info = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $info = $this->info;
        $data = [
            'titulo' => 'Aviso de Lembrete',
            'texto1' => 'Você tem um lembrete('.$info->NOME.') cadastrado para amanhã!.',
            'texto2' => 'Para consultar clique no botão abaixo!',
            'rota' => route('editarLembrete',['id'=> $info->ID]),
        ];
        return $this->from('to@email.com')->subject('Aviso de Lembrete')->view('emails.lembrete', $data );
    }
}
