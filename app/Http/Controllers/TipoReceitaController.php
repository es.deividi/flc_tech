<?php

namespace App\Http\Controllers;

use App\Models\ModelTipoReceita;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;


class TipoReceitaController extends Controller
{
    public function viewListarTiposReceitas(){
        $model = new ModelTipoReceita();
        $data = [
            'records' => $model->getAllTiposReceitas(session()->get('Auth')['idUsuario'])
        ];
        return view('admin.tipoReceita.listar_tipos_receitas', $data);
    }

    public function viewCadastrarTipoReceita(){
        return view('admin.tipoReceita.cadastrar_tipo_receita');
    }

    public function salvarTipoReceita(Request $request){
        $request->validate(
            [
                'nome' => ['required', 'min:3', 'max:255'],
            ],
            [
                'nome.required' => 'O campo nome é obrigatório',
                'nome.min' => 'O campo nome deve ter no minimo :min caracteres',
                'nome.max' => 'O campo nome deve ter no máximo :max caracteres',
            ]
        );

        $registro = new ModelTipoReceita();
        if(!$registro->validarTipoReceita($request)){
            $errors = new MessageBag();
            $errors->add('default', 'Já existe um registro com os dados informados');
            return redirect()->to('/cadastrarTipoReceita')->withInput($request->input())->withErrors($errors);
        }else{
            return redirect('/listarTiposReceitas')->with('message', 'Registro inserido com sucesso!');;
        }
    }

    public function viewEditarTipoReceita(Request $request) {
        $model = new ModelTipoReceita();
        $data = [
            'record' => $model->getTipoReceita($request->route('id'))
        ];

        return view('admin.tipoReceita.editar_tipo_receita', $data);
    }    
    
    public function updateTipoReceita(Request $request){
        $request->merge(['id_usuario' => session()->get('Auth')['idUsuario']]);
        $model = new ModelTipoReceita();
        $valido = $model->atualizarTipoReceita($request);

        if($valido){
            return redirect('editarTipoReceita/'.$request->input('id'))->with('message', 'Registro atualizado!');
        }else{
            return redirect('editarTipoReceita/'.$request->input('id'))->with('erro', 'Já existe um registro com os dados informados!');
        }
    }

    public function deleteTipoReceita($request) {
        $model = new ModelTipoReceita();        
        $ret = $model->deleteTipoReceita($request);

        if($ret){
            return redirect('listarTiposReceitas')->with('message', 'Registro removido com sucesso!');
        }else{
            return redirect('listarTiposReceitas')->with('erro', 'Não foi possivel remover o registro! Verifique se não existem receitas vinculadas!');
        }

    }
}
