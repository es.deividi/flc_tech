<?php

namespace App\Http\Controllers;

use App\Models\ModelTipoDespesa;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;

class TipoDespesaController extends Controller
{
    public function viewListarTiposDespesas(){
        $model = new ModelTipoDespesa();
        $data = [
            'records' => $model->getAllTiposDespesas(session()->get('Auth')['idUsuario'])
        ];
        return view('admin.tipoDespesa.listar_tipos_despesas', $data);
    }

    public function viewCadastrarTipoDespesa(){
        return view('admin.tipoDespesa.cadastrar_tipo_despesa');
    }

    public function salvarTipoDespesa(Request $request){
        $request->validate(
            [
                'nome' => ['required', 'min:3', 'max:255'],
            ],
            [
                'nome.required' => 'O campo nome é obrigatório',
                'nome.min' => 'O campo nome deve ter no minimo :min caracteres',
                'nome.max' => 'O campo nome deve ter no máximo :max caracteres',
            ]
        );

        $registro = new ModelTipoDespesa();
        if(!$registro->validarTipoDespesa($request)){
            $errors = new MessageBag();
            $errors->add('default', 'Já existe um registro com os dados informados');
            return redirect()->to('/cadastrarTipoDespesa')->withInput($request->input())->withErrors($errors);
        }else{
            return redirect('/listarTiposDespesas')->with('message', 'Registro inserido com sucesso!');;
        }
    }

    public function viewEditarTipoDespesa(Request $request) {
        $model = new ModelTipoDespesa();
        $data = [
            'record' => $model->getTipoDespesa($request->route('id'))
        ];

        return view('admin.tipoDespesa.editar_tipo_despesa', $data);
    }    
    
    public function updateTipoDespesa(Request $request){
        $request->merge(['id_usuario' => session()->get('Auth')['idUsuario']]);
        $model = new ModelTipoDespesa();
        $valido = $model->atualizarTipoDespesa($request);

        if($valido){
            return redirect('editarTipoDespesa/'.$request->input('id'))->with('message', 'Registro atualizado!');
        }else{
            return redirect('editarTipoDespesa/'.$request->input('id'))->with('erro', 'Já existe um registro com os dados informados!');
        }
    }

    public function deleteTipoDespesa($request) {
        $model = new ModelTipoDespesa();        
        $ret = $model->deleteTipoDeDespesa($request);

        if($ret){
            return redirect('listarTiposDespesas')->with('message', 'Registro removido com sucesso!');
        }else{
            return redirect('listarTiposDespesas')->with('erro', 'Não foi possivel remover o registro! Verifique se não existem despesas vinculadas!');
        }

    }
}
