<?php

namespace App\Http\Controllers;

use App\Models\ModelLembrete;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;

class LembreteController extends Controller
{
    public function viewListarLembretes(){
        $model = new ModelLembrete();
        $data = [
            'records' => $model->getAllLembretes(session()->get('Auth')['idUsuario'])
        ];
        return view('admin.lembrete.listar_lembretes',$data);
    }

    public function viewCadastrarLembrete(){
        return view('admin.lembrete.cadastrar_lembrete');
    }

    public function salvarLembrete(Request $request){
        $request->validate(
            [
                'nome' => ['required', 'min:3', 'max:255'],
                'email' => ['required', 'email'],
                'data' => ['required', 'date_format:d/m/Y'],
            ],
            [
                'nome.required' => 'O campo nome é obrigatório',
                'nome.min' => 'O campo nome deve ter no minimo :min caracteres',
                'nome.max' => 'O campo nome deve ter no máximo :max caracteres',
                'email.required' => 'O campo email é obrigatório',
                'data.required' => 'O campo data é obrigatório',
                'data.date_format' => 'O campo data precisa estar no formato d/m/Y',
            ]
        );
        $request->merge(['id_usuario' => session()->get('Auth')['idUsuario']]);
        $model = new ModelLembrete();
        if(!$model->validarLembrete($request)){
            $errors = new MessageBag();
            $errors->add('default', 'Já existe um registro com os dados informados');
            return redirect()->to('/cadastrarLembrete')->withInput($request->input())->withErrors($errors);
        }else{
            return redirect('/listarLembretes')->with('message', 'Registro inserido com sucesso!');;
        }
    }

    public function viewEditarLembrete(Request $request) {
        $model = new ModelLembrete();
        $data = [
            'record' => $model->getLembrete($request->route('id'))
        ];
        return view('admin.lembrete.editar_lembrete',$data);
    }    
    
    public function updateLembrete(Request $request){
        $request->merge(['id_usuario' => session()->get('Auth')['idUsuario']]);
        $model = new ModelLembrete();
        $valido = $model->atualizarLembrete($request);
        if($valido){
            return redirect('editarLembrete/'.$request->input('id'))->with('message', 'Registro atualizado!');
        }else{
            return redirect('editarLembrete/'.$request->input('id'))->with('erro', 'Já existe um registro com os dados informados!');
        }
    }

    public function deleteLembrete($request) {
        $model = new ModelLembrete();        
        $ret = $model->deleteLembrete($request);

        if($ret){
            return redirect('listarLembretes')->with('message', 'Registro removido com sucesso!');
        }else{
            return redirect('listarLembretes')->with('erro', 'Não foi possivel remover o registro!');
        }
    }

    public function lembretesAlterta(){
        $model = new ModelLembrete();
        $data = $model->getAllLembretes(session()->get('Auth')['idUsuario']);
        return response()->json(array('data'=> $data), 200);
    }

    public function avisoLembrete(){
        $model = new ModelLembrete();
        $model->avisoLembrete();
    }
}
