<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ModelReceita;
use App\Models\ModelTipoReceita;
use Illuminate\Support\MessageBag;

class ReceitaController extends Controller
{
    public function viewListarReceitas(){
        $modelReceita = new ModelReceita();
        $data = [
            'records' => $modelReceita->getAllReceitas(session()->get('Auth')['idUsuario']),
        ];
        return view('admin.receita.listar_receitas',$data);
    }

    public function viewCadastrarReceita(){
        $modelTipoReceita = new ModelTipoReceita();
        $data = [
            'tiposReceitas' => $modelTipoReceita->getAllTiposReceitas(session()->get('Auth')['idUsuario'])
        ];
        return view('admin.receita.cadastrar_receita', $data);
    }

    public function salvarReceita(Request $request){
        $request->validate(
            [
                'nome' => ['required', 'min:3', 'max:255'],
                'tipoReceita' => ['required'],
                'dataRecebimento' => ['required', 'date_format:d/m/Y'],
                'valor' => ['required']
            ],
            [
                'nome.required' => 'O campo nome é obrigatório',
                'nome.min' => 'O campo nome deve ter no minimo :min caracteres',
                'nome.max' => 'O campo nome deve ter no máximo :max caracteres',
                'tipoReceita.required' => 'O campo Tipo de receita é obrigatório',
                'dataRecebimento.required' => 'O campo data de recebimento é obrigatório',
                'dataRecebimento.date_format' => 'O campo data de recebimento precisa estar no formato d/m/Y',
                'valor.required' => 'O campo valor é obrigatório'
            ]
        );
        $request->merge(['id_usuario' => session()->get('Auth')['idUsuario']]);

        $model = new ModelReceita();
        if(!$model->validarReceita($request)){
            $errors = new MessageBag();
            $errors->add('default', 'Já existe um registro com os dados informados');
            return redirect()->to('/cadastrarReceita')->withInput($request->input())->withErrors($errors);
        }else{
            return redirect('/listarReceitas')->with('message', 'Registro inserido com sucesso!');;
        }
    }

    public function viewEditarReceita(Request $request) {
        $model = new ModelReceita();
        $modelTipoReceita = new ModelTipoReceita();
        $data = [
            'record' => $model->getReceita($request->route('id')),
            'tiposReceita' => $modelTipoReceita->getAllTiposReceitas(session()->get('Auth')['idUsuario'])
        ];
        return view('admin.receita.editar_receita', $data);
    }    
    
    public function updateReceita(Request $request){
        $request->merge(['id_usuario' => session()->get('Auth')['idUsuario']]);
        $model = new ModelReceita();
        $valido = $model->atualizarReceita($request);
        if($valido){
            return redirect('editarReceita/'.$request->input('id'))->with('message', 'Registro atualizado!');
        }else{
            return redirect('editarReceita/'.$request->input('id'))->with('erro', 'Já existe um registro com os dados informados!');
        }
    }

    public function deleteReceita($request) {
        $model = new ModelReceita();        
        $ret = $model->deleteReceita($request);

        if($ret){
            return redirect('listarReceitas')->with('message', 'Registro removido com sucesso!');
        }else{
            return redirect('listarReceitas')->with('erro', 'Não foi possivel remover o registro!');
        }
    }
}
