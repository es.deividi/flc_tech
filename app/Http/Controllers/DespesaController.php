<?php

namespace App\Http\Controllers;

use App\Models\ModelDespesa;
use App\Models\ModelTipoDespesa;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;

class DespesaController extends Controller
{
    public function viewListarDespesas(){
        $model = new ModelDespesa();
        $data = [
            'records' => $model->getAllDespesas(session()->get('Auth')['idUsuario']),
        ];
        return view('admin.despesa.listar_despesas',$data);
    }

    public function viewCadastrarDespesa(){
        $modelTipoReceita = new ModelTipoDespesa();
        $data = [
            'tiposDespesas' => $modelTipoReceita->getAllTiposDespesas(session()->get('Auth')['idUsuario'])
        ];
        return view('admin.despesa.cadastrar_despesa', $data);
    }

    public function salvarDespesa(Request $request){
        $request->validate(
            [
                'nome' => ['required', 'min:3', 'max:255'],
                'tipoDespesa' => ['required'],
                'dataVencimento' => ['required', 'date_format:d/m/Y'],
                'valor' => ['required']
            ],
            [
                'nome.required' => 'O campo nome é obrigatório',
                'nome.min' => 'O campo nome deve ter no minimo :min caracteres',
                'nome.max' => 'O campo nome deve ter no máximo :max caracteres',
                'tipoDespesa.required' => 'O campo Tipo de receita é obrigatório',
                'dataVencimento.date_format' => 'O campo data de vencimento precisa estar no formato d/m/Y',
                'valor.required' => 'O campo valor é obrigatório'
            ]
        );
        $request->merge(['id_usuario' => session()->get('Auth')['idUsuario']]);
        $model = new ModelDespesa();
        if(!$model->validarDespesa($request)){
            $errors = new MessageBag();
            $errors->add('default', 'Já existe um registro com os dados informados');
            return redirect()->to('/cadastrarDespesa')->withInput($request->input())->withErrors($errors);
        }else{
            return redirect('/listarDespesas')->with('message', 'Registro inserido com sucesso!');;
        }
    }

    public function viewEditarDespesa(Request $request) {
        $model = new ModelDespesa();
        $modelTipoDespesa = new ModelTipoDespesa();
        $data = [
            'record' => $model->getDespesa($request->route('id')),
            'tiposDespesa' => $modelTipoDespesa->getAllTiposDespesas(session()->get('Auth')['idUsuario'])
        ];
        return view('admin.despesa.editar_despesa', $data);
    }    
    
    public function updateDespesa(Request $request){
        $request->merge(['id_usuario' => session()->get('Auth')['idUsuario']]);
        $model = new ModelDespesa();
        $valido = $model->atualizarDespesa($request);
        if($valido){
            return redirect('editarDespesa/'.$request->input('id'))->with('message', 'Registro atualizado!');
        }else{
            return redirect('editarDespesa/'.$request->input('id'))->with('erro', 'Já existe um registro com os dados informados!');
        }
    }

    public function deleteDespesa($request) {
        $model = new ModelDespesa();        
        $ret = $model->deleteDespesa($request);

        if($ret){
            return redirect('listarDespesas')->with('message', 'Registro removido com sucesso!');
        }else{
            return redirect('listarDespesas')->with('erro', 'Não foi possivel remover o registro!');
        }

    }

    public function avisoVencimento(){
        $model = new ModelDespesa();
        $model->avisoVencimento();
    }
}
