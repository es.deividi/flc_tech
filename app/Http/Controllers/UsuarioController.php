<?php

namespace App\Http\Controllers;

use App\Models\ModelUsuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Hash;

class UsuarioController extends Controller
{
    public function viewFormulariologin(){
        return view('formularios.login');
    }
    
    public function viewRecuperarSenha(){
        return view('formularios.recuperar_senha');
    }

    public function viewCriarConta(){
        return view('formularios.criar_conta');
    }

    public function auth(Request $request){
        $request->validate(
            [
                'email' => ['required', 'email'],
                'password' => ['required', 'min:6']
            ],
            [
                'email.required' => 'O campo usuário é obrigatório',
                'email.email' => 'O campo usuário deve ser um email',
                'password.required' => 'O campo password é obrigatório',
                'password.min' => 'A senha tem no minimo :min caracteres'
            ]
        );

        $usuario = new ModelUsuario();
        if(!$usuario->validaLogin($request)){
            $errors = new MessageBag();
            $errors->add('default', 'Usuario não encontrado ou senha inválida!');
            return redirect()->to('/')->withInput($request->input())->withErrors($errors);
        }else{
            //dd($request->session()->get('Auth'));
            return redirect('/dashboard');
        }
    }

    public function create(Request $request){
        $request->validate(
            [
                'nome' => ['required', 'min:3', 'max:255'],
                'email' => ['required', 'email'],
                'password' => ['required', 'same:password2'],
                'password2' => ['required','same:password'],
            ],
            [
                'nome.required' => 'O campo nome é obrigatório',
                'nome.min' => 'O campo nome deve ter no minimo :min caracteres',
                'nome.max' => 'O campo nome deve ter no máximo :max caracteres',
                'email.required' => 'O campo email é obrigatório',
                'email.email' => 'O campo email precisa de um email valido',
                'password.required' => 'O campo senha é obrigatório',
                'password2.required' => 'A confirmação de senha é obrigatória',
                'password.same' => 'As senhas informadas são diferentes',
                'password2.same' => 'As senhas informadas são diferentes',
            ]
        );
        if(!empty($request->all())){
            DB::insert('INSERT INTO USUARIO VALUES(0, ?, ?, ?)', [$request->nome, $request->email, sha1($request->password)]);
            return redirect('/');
        }else{
            
        }
    }

    public function logout(){
        session()->forget('Auth');
        return redirect('/');
     }

     public function emailRecuperarSenha(Request $request){
        $usuario = new ModelUsuario();
        $usuario->emailRecuperarSenha($request->input('email'));
        return back()->with('message', 'Você recebera em seu email um link para redefinir sua senha!');
     }

     public function recuperarSenha(){
        return view('formularios.alterar_senha');
     }

     public function updateSenha(Request $request){
                $request->validate(
            [
                'email' => ['required', 'email'],
                'password' => ['required', 'same:password2'],
                'password2' => ['required','same:password'],
            ],
            [
                'email.required' => 'O campo email é obrigatório',
                'email.email' => 'O campo email precisa de um email valido',
                'password.required' => 'O campo senha é obrigatório',
                'password2.required' => 'A confirmação de senha é obrigatória',
                'password.same' => 'As senhas informadas são diferentes',
                'password2.same' => 'As senhas informadas são diferentes',
            ]
        );
        
        $usuario = new ModelUsuario();
        $ret = $usuario->updateSenha($request);
        
        if($ret){
            return back()->with('message', 'Senha alterada com sucesso!');
        }else{
            return back()->with('erro', 'Não foi possivel alterar sua senha! Tente novamente mais tarde.');
        }
     }
}

