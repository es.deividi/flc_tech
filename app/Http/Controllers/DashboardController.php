<?php

namespace App\Http\Controllers;

use App\Models\ModelDashboard;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    
    public function viewDashboard(){
        $model = new ModelDashboard();
        $data = $model->cardsData();
        return view('admin.dashboard', $data);
    }

    public function chartData(){
        $model = new ModelDashboard();
        $data = $model->chartData();
        return response()->json(array('data'=> $data), 200);
    }

    public function export(){
        $model = new ModelDashboard();
        $data = $model->chartData();
        $report = $model->report($data);
        return back()->with($report);
    }
}
