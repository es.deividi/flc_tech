<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModelUtil extends Model
{
    public function inverterData($valor){
        return implode('-',array_reverse(explode('/', $valor)));
    }

    public function normalizarValor($valor){
        return str_replace(",", ".", str_replace(".","", $valor));
    }

    public function formatarValor($valor){
        return number_format($valor,2,",",".");
    }
}
