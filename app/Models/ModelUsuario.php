<?php

namespace App\Models;

use App\Mail\SendMailUser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ModelUsuario extends Model
{
    public function validaLogin($request){      
        $data = [
         'email' => $request->input('email'),
         'password' => sha1($request->input('password'))
        ];

        $login = DB::select('SELECT ID_USUARIO, NOME, EMAIL FROM USUARIO WHERE EMAIL = ? AND SENHA = ?', [$data['email'], $data['password']]);
        if(count($login) == 1){
            $login = $login[0];
            $request->session()->put('Auth',[
                    'idUsuario'=> $login->ID_USUARIO,
                    'nome' => $login->NOME,
                    'email' => $login->EMAIL
            ]);
            return true;
         }
         return false;
     }

     public function emailRecuperarSenha($request){
        $usuario = DB::select('SELECT NOME, EMAIL FROM USUARIO WHERE EMAIL = ? ', [$request]);
        if(count($usuario) == 1){
            Mail::to($request)->send(new SendMailUser($usuario));
        }
     }

     public function updateSenha($request){
        $usuario = DB::select('SELECT ID_USUARIO, NOME, EMAIL FROM USUARIO WHERE EMAIL = ? ', [$request->input('email')]);
        if(count($usuario) == 1){
            return DB::update('UPDATE USUARIO SET SENHA = ? WHERE ID_USUARIO = ?', [sha1($request->input('password')), $usuario[0]->ID_USUARIO]);
        }else{
            return false;
        }
     }
}
