<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class ModelReceita
{
    public function validarReceita($request){
        $util = new ModelUtil();
        $dataRecebimento =  $util->inverterData($request->input('dataRecebimento'));
        $valor = $util->normalizarValor($request->input('valor'));
        $data = [
            'nome' => $request->input('nome'),
            'observacao' => $request->input('observacao'),
            'tipoReceita' => $request->input('tipoReceita'),
            'dataRecebimento' => $dataRecebimento,
            'valor' => $valor,
            'id_usuario' => session()->get('Auth')['idUsuario']
        ];
        return $this->salvarReceita($data);
    }

    public function salvarReceita($request) {
        return DB::insert(
            'INSERT INTO RECEITA VALUES(0, ?, ?, ?, ?, ?, ?)', [
                $request['nome'],
                $request['observacao'],
                $request['tipoReceita'],
                $request['id_usuario'],
                $request['dataRecebimento'],
                $request['valor']
            ]
        );
    }
    
    public function atualizarReceita($request){
        $util = new ModelUtil();
        $dataRecebimento =  $util->inverterData($request->input('dataRecebimento'));
        $valor = $util->normalizarValor($request->input('valor'));
        $atualiza = true;
        if($atualiza){            
            DB::update(
                'UPDATE RECEITA SET DS_NOME = ?,
                OBSERVACAO = ?,
                ID_TIPO_RECEITA = ?,
                DATA_RECEBIMENTO = ?,
                VALOR = ?
                WHERE ID_RECEITA = ?', [
                    $request->input('nome'),
                    $request->input('observacao'),
                    $request->input('tipoReceita'),
                    $dataRecebimento,
                    $valor,
                    $request['id']
                ]);
            return true;
        }else{
            return false;
        }
    }

    public function getAllReceitas($request){
        return DB::select(
            'SELECT R.ID_RECEITA AS ID,
            R.DS_NOME AS NOME,
            TR.ID_TIPO_RECEITA AS TIPO_RECEITA,
            TR.DS_NOME AS DS_RECEITA,
            R.OBSERVACAO AS OBSERVACAO,
            DATE_FORMAT(R.DATA_RECEBIMENTO, "%d/%m/%Y") AS DATA_RECEBIMENTO,
            FORMAT(R.VALOR,2,"de_DE") AS VALOR            
            FROM RECEITA R 
            JOIN TIPORECEITA TR ON TR.ID_TIPO_RECEITA = R.ID_TIPO_RECEITA
            WHERE R.ID_USUARIO = ?
            ORDER BY R.DATA_RECEBIMENTO ASC
            ', [$request]);
    }
    
    public function getReceita($request){
        return DB::select(
            'SELECT R.ID_RECEITA AS ID,
            R.DS_NOME AS NOME,
            TR.ID_TIPO_RECEITA AS TIPO_RECEITA,
            R.OBSERVACAO AS OBSERVACAO,
            DATE_FORMAT(R.DATA_RECEBIMENTO, "%d/%m/%Y") AS DATA_RECEBIMENTO,
            R.VALOR
            FROM RECEITA R 
            JOIN TIPORECEITA TR ON TR.ID_TIPO_RECEITA = R.ID_TIPO_RECEITA
            WHERE R.ID_RECEITA = ?', [$request]);
    }

    public function deleteReceita($request){
        return DB::delete('DELETE FROM RECEITA WHERE ID_RECEITA = ?', [$request]);
    }

    public function receitaMensal($mes, $idUsuario){
        return DB::select(
            'SELECT 
                CASE 
                    WHEN SUM(VALOR) IS NOT NULL 
                        THEN SUM(VALOR) 
                    ELSE 0
                END AS VALOR
            FROM RECEITA WHERE MONTH(DATA_RECEBIMENTO) = ? AND ID_USUARIO = ?', [$mes, $idUsuario]
        );
    }

    public function receitaAnual($ano, $idUsuario){
        return DB::select(
            'SELECT 
                CASE 
                    WHEN SUM(VALOR) IS NOT NULL
                    THEN SUM(VALOR) 
                ELSE 0
            END AS VALOR
            FROM RECEITA WHERE YEAR(DATA_RECEBIMENTO) = ? AND ID_USUARIO = ?', [$ano, $idUsuario] 
        ) ;
    }

    public function receitasAgrupadasMes($ano, $idUsuario){
        return DB::select(
            'SELECT 
            MAX(
                CASE WHEN MES = 1 then VALOR ELSE 0 END
            ) AS "JAN",
            MAX(
                CASE WHEN MES = 2 then VALOR ELSE 0 END
            ) AS "FEV",
            MAX(
                CASE WHEN MES = 3 then VALOR ELSE 0 END
            ) AS "MAR",
            MAX(
                CASE WHEN MES = 4 then VALOR ELSE 0 END
            ) AS "ABR",
            MAX(
                CASE WHEN MES = 5 then VALOR ELSE 0 END
            ) AS "MAI",
            MAX(
                CASE WHEN MES = 6 then VALOR ELSE 0 END
            ) AS "JUN",
            MAX(
                CASE WHEN MES = 7 then VALOR ELSE 0 END
            ) AS "JUL",
            MAX(
                CASE WHEN MES = 8 then VALOR ELSE 0 END
            ) AS "AGO",
            MAX(
                CASE WHEN MES = 9 then VALOR ELSE 0 END
            ) AS "SET",
            MAX(
                CASE WHEN MES = 10 then VALOR ELSE 0 END
            ) AS "OUT",
            MAX(
                CASE WHEN MES = 11 then VALOR ELSE 0 END
            ) AS "NOV",
            MAX(
                CASE WHEN MES = 12 then VALOR ELSE 0 END
            ) AS "DEZ",
            ANO
            FROM
            (
				SELECT 
                ID_USUARIO,
                MONTH(DATA_RECEBIMENTO) as MES,
                YEAR(DATA_RECEBIMENTO) as ANO,
                SUM(VALOR) as VALOR
				FROM RECEITA
				GROUP BY YEAR(DATA_RECEBIMENTO), MONTH(DATA_RECEBIMENTO), ID_USUARIO
            ) TEMP
			WHERE ID_USUARIO = ? AND ANO = ?
            GROUP BY ID_USUARIO, ANO
            ', [$idUsuario, $ano]    
        );
    }
}
