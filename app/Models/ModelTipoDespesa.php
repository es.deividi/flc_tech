<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class ModelTipoDespesa
{
    public function validarTipoDespesa($request){
        $data = [
            'nome' => $request->input('nome'),
            'id_usuario' => session()->get('Auth')['idUsuario']
        ];
    
        $registro = DB::select('SELECT ID_TIPO_DESPESA, DS_NOME FROM TIPODESPESA WHERE DS_NOME = ? AND ID_USUARIO = ?', [$data['nome'], $data['id_usuario']]);

        if(count($registro) == 1){
            return false;
        }else{
            $this->salvarTipoDespesa($data);
            return true;
        }
    }

    public function salvarTipoDespesa($request){
        DB::insert('INSERT INTO TIPODESPESA VALUES(0, ?, ?)', [$request['nome'], $request['id_usuario']]);
    }

    public function atualizarTipoDespesa($request){
        $atualiza = true;
        $records = DB::select('SELECT ID_TIPO_DESPESA, DS_NOME FROM TIPODESPESA WHERE ID_USUARIO = ?', [$request['id_usuario']]);
        foreach($records as $record){
            if($record->DS_NOME == $request['nome']){
                if($record->ID_TIPO_DESPESA != $request['id']){
                    $atualiza = false;
                }
            }
        }
        if($atualiza){            
            DB::update('UPDATE TIPODESPESA SET DS_NOME = ? WHERE ID_TIPO_DESPESA = ?', [$request['nome'], $request['id']]);
            return true;
        }else{
            return false;
        }
    }

    public function getAllTiposDespesas($request){
        return DB::select('SELECT ID_TIPO_DESPESA AS ID, DS_NOME AS NOME FROM TIPODESPESA WHERE ID_USUARIO = ?', [$request]);
    }
    
    public function getTipoDespesa($request){
        return DB::select('SELECT ID_TIPO_DESPESA AS ID, DS_NOME AS NOME FROM TIPODESPESA WHERE ID_TIPO_DESPESA = ?', [$request]);
    }

    public function deleteTipoDeDespesa($request){
        $valida = DB::select('SELECT COUNT(ID_DESPESA) AS COUNT FROM DESPESA WHERE ID_TIPO_DESPESA = ?', [$request]);
        if($valida[0]->COUNT == 0){
            return DB::delete('DELETE FROM TIPODESPESA WHERE ID_TIPO_DESPESA = ?', [$request]);
        }else{
            return false;
        }
    }
}
