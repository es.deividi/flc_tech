<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailLembrete;

class ModelLembrete
{
    public function validarLembrete($request){
        $util = new ModelUtil();
        $data =  $util->inverterData($request->input('data'));
        $data = [
            'nome' => $request->input('nome'),
            'descricao' => $request->input('descricao'),
            'email' => $request->input('email'),
            'data' => $data,
            'id_usuario' => session()->get('Auth')['idUsuario'],
        ];

        $registro = DB::select(
            'SELECT ID_LEMBRETE,
            DS_NOME
            FROM LEMBRETE
            WHERE DS_NOME = ? AND ID_USUARIO = ?', [
                $data['nome'],
                $data['id_usuario']
            ]
        );

        if(count($registro) == 1){
            return false;
        }else{
            $this->salvarLembrete($data);
            return true;
        }
    }

    public function salvarLembrete($request) {
        return DB::insert(
            'INSERT INTO LEMBRETE VALUES(0, ?, ?, ?, ?, ?, ?)', [
                $request['nome'],
                $request['descricao'],
                $request['email'],
                $request['data'],
                0,
                $request['id_usuario']
            ]
        );
    }
    
    public function atualizarLembrete($request){
        $atualiza = true;
        $records = DB::select('SELECT ID_LEMBRETE, DS_NOME FROM LEMBRETE WHERE ID_USUARIO = ?', [$request['id_usuario']]);
        foreach($records as $record){
            if($record->DS_NOME == $request['nome']){
                if($record->ID_LEMBRETE != $request['id']){
                    $atualiza = false;
                }
            }
        }
        if($atualiza){            
            DB::update(
                'UPDATE LEMBRETE SET DS_NOME = ?,
                DESCRICAO = ?,
                EMAIL = ?,
                DATA = ?,
                AVISO = ?
                WHERE ID_LEMBRETE = ?', [
                    $request->input('nome'),
                    $request->input('descricao'),
                    $request->input('email'),
                    $request->input('data'),
                    0,
                    $request['id']
                ]
            );
            return true;
        }else{
            return false;
        }
    }

    public function getAllLembretes($request){
        return DB::select(
            'SELECT ID_LEMBRETE AS ID,
            DS_NOME AS NOME,
            DESCRICAO,
            EMAIL,
            DATA
            FROM LEMBRETE
            WHERE ID_USUARIO = ?', [$request]
        );
    }
    
    public function getLembrete($request){
        return DB::select( 
            'SELECT ID_LEMBRETE AS ID,
            DS_NOME AS NOME,
            DESCRICAO,
            EMAIL,
            DATA
            FROM LEMBRETE
            WHERE ID_LEMBRETE = ?', [$request]
        );
    }

    public function deleteLembrete($request){
        return DB::delete('DELETE FROM LEMBRETE WHERE ID_LEMBRETE = ?', [$request]);
    }

    public function avisoLembrete(){
        $lembretes = DB::select(
            'SELECT DS_NOME AS NOME,
            EMAIL,
            ID_LEMBRETE AS ID
            FROM LEMBRETE 
            WHERE ADDDATE(DATA,INTERVAL 0 DAY) <= CURDATE()
            AND AVISO = 0 
        ');

        foreach($lembretes as $lembrete){
            Mail::to($lembrete->EMAIL)->send(new SendMailLembrete($lembrete));
            DB::update('UPDATE LEMBRETE SET AVISO = ? WHERE ID_LEMBRETE = ?', [1, $lembrete->ID]);
        }
    }
}