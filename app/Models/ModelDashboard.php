<?php

namespace App\Models;

use App\Reports\Reports;

class ModelDashboard
{
    public function cardsData(){
        $util = new ModelUtil();
        $modelReceita = new ModelReceita();
        $modelDespesa = new ModelDespesa();
        $mes = date('m');
        $ano = date('Y');
        $idUsuario = session()->get('Auth')['idUsuario'];
        
        $data = [
            'receitaMensal' => $util->formatarValor($modelReceita->receitaMensal($mes, $idUsuario)[0]->VALOR),
            'receitaAnual'  => $util->formatarValor($modelReceita->receitaAnual($ano, $idUsuario)[0]->VALOR),
            'despesaMensal' => $util->formatarValor($modelDespesa->despesaMensal($mes, $idUsuario)[0]->VALOR),
            'despesaAnual'  => $util->formatarValor($modelDespesa->despesaAnual($ano, $idUsuario)[0]->VALOR),
        ];
       return $data;
    }
    
    public function chartData(){
        $modelReceita = new ModelReceita();
        $modelDespesa = new ModelDespesa();
        $ano = date('Y');
        $idUsuario = session()->get('Auth')['idUsuario'];
        $receitas = $modelReceita->receitasAgrupadasMes($ano, $idUsuario);
        $despesas = $modelDespesa->despesasAgrupadasMes($ano, $idUsuario);
        $data = [
            'receitas' => sizeof($receitas) > 0 ? $receitas[0] : [],
            'despesas'  => sizeof($despesas) > 0 ? $despesas[0] : [],
        ];
       return $data;
    }

    public function report($data){
        $report = new Reports();
        return $report->reportDashboard($data);
    }
}
