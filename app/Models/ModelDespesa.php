<?php

namespace App\Models;

use App\Mail\SendMailDespesa;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ModelDespesa
{
    public function validarDespesa($request){
        $util = new ModelUtil();
        $dataVencimento =  $util->inverterData($request->input('dataVencimento'));
        $valor = $util->normalizarValor($request->input('valor'));
        $data = [
            'nome' => $request->input('nome'),
            'observacao' => $request->input('observacao'),
            'tipoDespesa' => $request->input('tipoDespesa'),
            'id_usuario' => session()->get('Auth')['idUsuario'],
            'dataVencimento' => $dataVencimento,
            'valor' => $valor
        ];
        
        return $this->salvarDespesa($data);
    }

    public function salvarDespesa($request) {
        return DB::insert(
            'INSERT INTO DESPESA VALUES(0, ?, ?, ?, ?, ?, ?, ?)', [
                $request['nome'],
                $request['observacao'],
                $request['tipoDespesa'],
                $request['id_usuario'],
                $request['dataVencimento'],
                $request['valor'],
                0
            ]
        );
    }
    
    public function atualizarDespesa($request){
        return DB::update(
            'UPDATE DESPESA SET DS_NOME = ?,
            OBSERVACAO = ?,
            ID_TIPO_DESPESA = ?,
            AVISO = ?
            WHERE ID_DESPESA = ?', [
                $request->input('nome'),
                $request->input('observacao'),
                $request->input('tipoDespesa'),
                0,
                $request['id']
            ]
        );
    }

    public function getAllDespesas($request){
        return DB::select(
            'SELECT D.ID_DESPESA AS ID,
            D.DS_NOME AS NOME,
            TD.ID_TIPO_DESPESA AS TIPO_DESPESA,
            TD.DS_NOME AS DS_DESPESA,
            D.OBSERVACAO AS OBSERVACAO,
            DATE_FORMAT(D.DATA_VENCIMENTO, "%d/%m/%Y") AS DATA_VENCIMENTO,
            FORMAT(D.VALOR,2,"de_DE") AS VALOR            
            FROM DESPESA D 
            JOIN TIPODESPESA TD ON TD.ID_TIPO_DESPESA = D.ID_TIPO_DESPESA
            WHERE D.ID_USUARIO = ?
            ORDER BY D.DATA_VENCIMENTO ASC
            ', [$request]
        );
    }
    
    public function getDespesa($request){
        return DB::select( 
            'SELECT D.ID_DESPESA AS ID,
            D.DS_NOME AS NOME,
            TD.ID_TIPO_DESPESA AS TIPO_DESPESA,
            TD.DS_NOME AS DS_DESPESA,
            D.OBSERVACAO AS OBSERVACAO,
            DATE_FORMAT(D.DATA_VENCIMENTO, "%d/%m/%Y") AS DATA_VENCIMENTO,
            FORMAT(D.VALOR,2,"de_DE") AS VALOR            
            FROM DESPESA D 
            JOIN TIPODESPESA TD ON TD.ID_TIPO_DESPESA = D.ID_TIPO_DESPESA
            WHERE D.ID_DESPESA = ?', [$request]
        );
    }

    public function deleteDespesa($request){
        return DB::delete('DELETE FROM DESPESA WHERE ID_DESPESA = ?', [$request]);
    }

    public function despesaMensal($mes, $idUsuario){
        return DB::select(
            'SELECT 
                CASE 
                    WHEN SUM(VALOR) IS NOT NULL 
                        THEN SUM(VALOR) 
                    ELSE 0
                END AS VALOR
            FROM DESPESA WHERE MONTH(DATA_VENCIMENTO) = ? AND ID_USUARIO = ?', [$mes, $idUsuario]
        );
    }

    public function despesaAnual($ano, $idUsuario){
        return DB::select(
            'SELECT 
                CASE 
                    WHEN SUM(VALOR) IS NOT NULL
                    THEN SUM(VALOR) 
                ELSE 0
            END AS VALOR
            FROM DESPESA WHERE YEAR(DATA_VENCIMENTO) = ? AND ID_USUARIO = ?', [$ano, $idUsuario] 
        ) ;
    }

    public function despesasAgrupadasMes($ano, $idUsuario){
        return DB::select(
            'SELECT 
                MAX(
                    CASE WHEN MES = 1 then VALOR ELSE 0 END
                ) AS "JAN",
                MAX(
                    CASE WHEN MES = 2 then VALOR ELSE 0 END
                ) AS "FEV",
                MAX(
                    CASE WHEN MES = 3 then VALOR ELSE 0 END
                ) AS "MAR",
                MAX(
                    CASE WHEN MES = 4 then VALOR ELSE 0 END
                ) AS "ABR",
                MAX(
                    CASE WHEN MES = 5 then VALOR ELSE 0 END
                ) AS "MAI",
                MAX(
                    CASE WHEN MES = 6 then VALOR ELSE 0 END
                ) AS "JUN",
                MAX(
                    CASE WHEN MES = 7 then VALOR ELSE 0 END
                ) AS "JUL",
                MAX(
                    CASE WHEN MES = 8 then VALOR ELSE 0 END
                ) AS "AGO",
                MAX(
                    CASE WHEN MES = 9 then VALOR ELSE 0 END
                ) AS "SET",
                MAX(
                    CASE WHEN MES = 10 then VALOR ELSE 0 END
                ) AS "OUT",
                MAX(
                    CASE WHEN MES = 11 then VALOR ELSE 0 END
                ) AS "NOV",
                MAX(
                    CASE WHEN MES = 12 then VALOR ELSE 0 END
                ) AS "DEZ",
                ANO
            FROM
            (
				SELECT 
                ID_USUARIO,
                MONTH(DATA_VENCIMENTO) as MES,
                YEAR(DATA_VENCIMENTO) as ANO,
                SUM(VALOR) as VALOR
				FROM DESPESA
				GROUP BY YEAR(DATA_VENCIMENTO), MONTH(DATA_VENCIMENTO), ID_USUARIO
            ) TEMP
			WHERE ID_USUARIO = ? AND ANO = ?
            GROUP BY ID_USUARIO, ANO
            ', [$idUsuario, $ano]  
        );
    }

    public function avisoVencimento(){
        $despesas = DB::select(
            'SELECT D.ID_DESPESA AS ID,
            D.DS_NOME AS NOME,
            D.OBSERVACAO AS OBSERVACAO,
            U.NOME AS USUARIO,
            U.EMAIL AS EMAIL,
            DATE_FORMAT(D.DATA_VENCIMENTO, "%d/%m/%Y") AS DATA_VENCIMENTO,
            FORMAT(D.VALOR,2,"de_DE") AS VALOR            
            FROM DESPESA D 
            JOIN USUARIO U ON U.ID_USUARIO = D.ID_USUARIO
            WHERE ADDDATE(DATA_VENCIMENTO,INTERVAL 0 DAY) <= CURDATE()
            AND D.AVISO = 0
        ');

        foreach($despesas as $despesa){
            Mail::to($despesa->EMAIL)->send(new SendMailDespesa($despesa));
            DB::update('UPDATE DESPESA SET AVISO = ? WHERE ID_DESPESA = ?', [1, $despesa->ID]);
        }

    }
}
