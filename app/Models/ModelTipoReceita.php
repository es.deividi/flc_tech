<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class ModelTipoReceita
{
    public function validarTipoReceita($request){
        $data = [
            'nome' => $request->input('nome'),
            'id_usuario' => session()->get('Auth')['idUsuario']
        ];
    
        $registro = DB::select('SELECT ID_TIPO_RECEITA, DS_NOME FROM TIPORECEITA WHERE DS_NOME = ? AND ID_USUARIO = ?', [$data['nome'], $data['id_usuario']]);

        if(count($registro) == 1){
            return false;
        }else{
            $this->salvarTipoReceita($data);
            return true;
        }
    }

    public function salvarTipoReceita($request){
        DB::insert('INSERT INTO TIPORECEITA VALUES(0, ?, ?)', [$request['nome'], $request['id_usuario']]);
    }

    public function atualizarTipoReceita($request){
        $atualiza = true;
        $records = DB::select('SELECT ID_TIPO_RECEITA, DS_NOME FROM TIPORECEITA WHERE ID_USUARIO = ?', [$request['id_usuario']]);
        foreach($records as $record){
            if($record->DS_NOME == $request['nome']){
                if($record->ID_TIPO_RECEITA != $request['id']){
                    $atualiza = false;
                }
            }
        }
        if($atualiza){            
            DB::update('UPDATE TIPORECEITA SET DS_NOME = ? WHERE ID_TIPO_RECEITA = ?', [$request['nome'], $request['id']]);
            return true;
        }else{
            return false;
        }
    }

    public function getAllTiposReceitas($request){
        return DB::select('SELECT ID_TIPO_RECEITA AS ID, DS_NOME AS NOME FROM TIPORECEITA WHERE ID_USUARIO = ?', [$request]);
    }
    
    public function getTipoReceita($request){
        return DB::select('SELECT ID_TIPO_RECEITA AS ID, DS_NOME AS NOME FROM TIPORECEITA WHERE ID_TIPO_RECEITA = ?', [$request]);
    }

    public function deleteTipoReceita($request){
        $valida = DB::select('SELECT COUNT(id_receita) AS COUNT from RECEITA where ID_TIPO_RECEITA = ?', [$request]);
        if($valida[0]->COUNT == 0){
            return DB::delete('DELETE FROM TIPORECEITA WHERE ID_TIPO_RECEITA = ?', [$request]);
        }else{
            return false;
        }
    }
}
