<?php

namespace App\Console\Commands;

use App\Http\Controllers\DespesaController;
use Illuminate\Console\Command;

class avisoVencimento extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'avisoVencimento:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envio de emails avisando vencimento';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $despesa = new DespesaController();
        $despesa->avisoVencimento();
        return 0;
    }
}
