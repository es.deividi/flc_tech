<?php

namespace App\Console\Commands;

use App\Http\Controllers\LembreteController;
use Illuminate\Console\Command;

class avisoLembrete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'avisoLembrete:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envio de emails avisando lembretes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $despesa = new LembreteController();
        $despesa->avisoLembrete();
        return 0;
    }
}
